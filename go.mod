module gitlab.com/starius/changeless

go 1.15

require (
	github.com/shopspring/decimal v0.0.0-20180709203117-cd690d0c9e24
	gitlab.com/starius/knapsack v0.0.0-20210103230600-6c90a1e087b8
)
