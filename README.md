# changeless

Select UTXOs to make a transaction without change.

Installation:

```
$ go get gitlab.com/starius/changeless
```

To run put all candidate coins' balances into a file and run the tool:

```
$ echo "0.1 0.002 0.00832423" > coins.txt
$ changeless -amount 0.01 -coins coins.txt
Processing item 3/3.
0.002 + 0.00832423 = 0.01032423
Tx size: 342 vBytes. Total fees: 0.00032423 BTC (94.8 sats/vByte).
```

## How to specify fees

If you choose coins for a transaction, you must also include a certain fee
in you transaction. The fee depends on the number of inputs (the tool accounts
for this automatically) as well as input size, output size, the number of
outputs and desired fee rate. You can change all these parameters via flags
(see them and reference values for input and output sizes: `changeless --help`).

If you don't want to account for fees, pass `-fee 0` to the tool.

## More complex example

I put 20 random balances from 0.0 to 1.0 into the file:
```
$ for i in {1..20}; do python -c 'import random; print("%.8f"%random.random())' >> coins.txt; done
$ sort coins.txt | paste -s | fold
0.03992843      0.05065794      0.22455962      0.26885163      0.38492344
0.52585970      0.53095603      0.54390228      0.56416635      0.56763977
0.62541372      0.63871118      0.64886460      0.69449311      0.71667450
0.79022429      0.79846454      0.79910291      0.87441572      0.91922843
```

Now imagine that I want to assemble 1.23456789 from some of those coins:
```
$ changeless -amount 1.23456789 -coins coins.txt
Processing item 20/20.
0.05065794 + 0.38492344 + 0.79910291 = 1.23468429
Tx size: 490 vBytes. Total fees: 0.0001164 BTC (23.8 sats/vByte).
```

The amount is slightly higher. Extra 0.00011640 will cover transaction fees.
The more coins you have, the more closely you can assemble the desired amount.

Let me now assemle 5.00000000 from the same coins:

```
$ changeless -amount 5.00000000 -coins coins.txt
Processing item 20/20.
0.03992843 + 0.38492344 + 0.53095603 + 0.54390228 + 0.56763977 + 0.6488646 +
    0.69449311 + 0.79022429 + 0.79910291 = 5.00003486
Tx size: 1378 vBytes. Total fees: 0.00003486 BTC (2.5 sats/vByte).
```

Not bad, right?

## Even more complex example

Lets make another set of random balances of size 50:

```
$ for i in {1..50}; do python -c 'import random; print("%.8f"%random.random())' >> coins50.txt; done
$ sort coins50.txt | paste -s | fold
0.01331611      0.03906237      0.04847086      0.08453118      0.09748168
0.10395389      0.10619825      0.12156721      0.12923149      0.13587973
0.14798976      0.16053788      0.19011834      0.21570038      0.21946913
0.31861430      0.33435508      0.33718842      0.33789473      0.35976748
0.37360122      0.44944553      0.47572926      0.49927495      0.50992142
0.53062326      0.53079433      0.53542072      0.54715225      0.55019714
0.55313907      0.56656642      0.56673333      0.65879650      0.66228482
0.68424322      0.70436496      0.75638055      0.79095597      0.82438005
0.83684407      0.85151564      0.86862948      0.90054250      0.90239402
0.91636213      0.93087757      0.93579251      0.97207439      0.98248384
```

Now lets assemble exactly 5.00000000 and 10.00000000 from these coins:

```
$ changeless -amount 5 -coins coins50.txt
Processing item 50/50.
0.10619825 + 0.12923149 + 0.47572926 + 0.54715225 + 0.55019714 + 0.56656642 + 0.79095597 + 0.85151564 +
    0.98248384 = 5.00003026
Tx size: 1378 vBytes. Total fees: 0.00003026 BTC (2.2 sats/vByte).

$ changeless -amount 10 -coins coins50.txt
Processing item 50/50.
0.09748168 + 0.33435508 + 0.47572926 + 0.53542072 + 0.66228482 + 0.70436496 + 0.75638055 + 0.82438005 +
    0.9005425 + 0.90239402 + 0.91636213 + 0.93579251 + 0.97207439 + 0.98248384 = 10.00004651
Tx size: 2118 vBytes. Total fees: 0.00004651 BTC (2.2 sats/vByte).
```

It took several minutes and large amount of RAM (~15 GB and ~10 GB) but
produced amazing results: exact amount is assembled, up to last digit
(if you deduce transaction fee: 5.00003026 - 0.00003026 = 5.00000000).
