package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"regexp"
	"strings"

	"github.com/shopspring/decimal"
	"gitlab.com/starius/knapsack"
)

var (
	coinsFile  = flag.String("coins", "/dev/stdin", "File with input balances")
	amount     = flag.String("amount", "0.01", "Amount to collect from the coins")
	inputSize  = flag.Float64("input-size", 148, "Size of input in vBytes (P2PKH: 148, P2SH-P2WPKH: 109.5, P2WPKH: 68.5)")
	outputSize = flag.Float64("output-size", 34, "Size of output in vBytes (P2PKH: 34, P2SH-P2WPKH: 32, P2WPKH: 31)")
	outputs    = flag.Int("outputs", 1, "Number of transaction outputs")
	feeRate    = flag.Float64("fee", 2.2, "Transaction fee, sats/vByte")
	maxmem     = flag.Int("maxmem", 512*1024*1024, "Max memory used for knapsack table, bytes")
)

func logger(message string, args ...interface{}) {
	fmt.Printf(message, args...)
}

func SelectCoinsWithNoChange(coins []int64, amount int64) []int64 {
	sum := int64(0)
	for _, coin := range coins {
		sum += coin
	}
	if amount > sum {
		panic("amount is too high")
	}
	excluded := knapsack.Knapsack(coins, sum-amount, int64(*maxmem), logger)
	excludedSet := make(map[int64]int)
	for _, coin := range excluded {
		excludedSet[coin] += 1
	}
	result := make([]int64, 0, len(coins)-len(excluded))
	for _, coin := range coins {
		if excludedSet[coin] == 0 {
			result = append(result, coin)
		} else {
			excludedSet[coin] -= 1
		}
	}
	return result
}

var (
	balanceRe = regexp.MustCompile(`[0-9]+\.[0-9]+`)
	satsInBit = decimal.New(1, 8)
)

func ToSats(balance string) int64 {
	decimalValue, err := decimal.NewFromString(balance)
	if err != nil {
		panic(err)
	}
	return decimalValue.Mul(satsInBit).IntPart()
}

func FromSats(sats int64) string {
	return decimal.New(sats, 0).Div(satsInBit).String()
}

// Transaction size info: https://bitcoin.stackexchange.com/a/84006

const headerSize = 12

func main() {
	flag.Parse()

	coinsData, err := ioutil.ReadFile(*coinsFile)
	if err != nil {
		panic(err)
	}
	var coins []int64
	for _, coinData := range balanceRe.FindAll(coinsData, -1) {
		coins = append(coins, ToSats(string(coinData)))
	}

	amountSats := ToSats(*amount)

	// Deduce transaction fees from coins.
	feePerInput := int64(*inputSize * *feeRate)
	additionalSize := headerSize + *outputSize*float64(*outputs)
	var coinsAfterFee []int64
	toOriginal := make(map[int64]int64)
	for _, coin := range coins {
		coin2 := coin - feePerInput
		coinsAfterFee = append(coinsAfterFee, coin2)
		toOriginal[coin2] = coin
	}
	amountAfterFee := amountSats + int64(*feeRate*additionalSize)

	selected := SelectCoinsWithNoChange(coinsAfterFee, amountAfterFee)

	sum2 := int64(0)
	for _, coin2 := range selected {
		sum2 += coin2
	}
	if sum2 < amountAfterFee {
		panic("sum of coins < amount")
	}

	var coinsStrings []string
	sum := int64(0)
	for _, coin2 := range selected {
		coin, has := toOriginal[coin2]
		if !has {
			panic("unknown coin")
		}
		sum += coin
		coinsStrings = append(coinsStrings, FromSats(coin))
	}

	fmt.Println(strings.Join(coinsStrings, " + ") + " = " + FromSats(sum))

	totalTxSize := int64(float64(len(selected))**inputSize + additionalSize)
	totalFees := sum - amountSats
	feePerVByte := float64(totalFees) / float64(totalTxSize)

	fmt.Printf("Tx size: %d vBytes. Total fees: %s BTC (%.1f sats/vByte).\n", totalTxSize, FromSats(totalFees), feePerVByte)
}
